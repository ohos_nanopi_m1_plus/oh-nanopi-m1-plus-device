#!/usr/bin/env python3
import sys
import os
import os.path
import subprocess
import multiprocessing
import shutil
import pathlib

os.environ['ARCH'] = 'arm'
os.environ['CROSS_COMPILE'] = 'arm-linux-'

defconfig = 'ohos_sunxi_defconfig'
kernel_dir = os.path.abspath('../../kernel/linux-nanopim1plus')
script_dir = os.path.abspath(os.path.dirname(__file__))
bootimgsize = 64*1024*1024
command = sys.argv[1]
output_dir = sys.argv[2]

def remove_file(name):
    try:
        os.unlink(name)
    except FileNotFoundError:
        return

def remove_directory(name):
    try:
        shutil.rmtree(name)
    except FileNotFoundError:
        pass

def make_kernel_all():
    oldpwd = os.getcwd()
    os.chdir(kernel_dir)

    zimage = os.path.join(output_dir, 'zImage')
    remove_file(zimage)
    nproc = multiprocessing.cpu_count()
    subprocess.run(F'touch .scmversion', shell=True, check=True)
    subprocess.run(F'make {defconfig} ARCH=arm CROSS_COMPILE=arm-linux-', shell=True, check=True)
    subprocess.run(F'make -j{nproc+1} zImage dtbs ARCH=arm CROSS_COMPILE=arm-linux-', shell=True, check=True)
    shutil.copy(
        os.path.join('arch/arm/boot/zImage'),
        zimage
    )

    os.chdir(oldpwd)

def install_kernel_modules():
    modules_dir = os.path.join(output_dir, 'kernel_modules')
    remove_directory(modules_dir)
    kolist = [
        'drivers/net/wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko',
        'drivers/net/wireless/broadcom/brcm80211/brcmutil/brcmutil.ko',
    ]
    os.mkdir(modules_dir)
    #for ko in kolist:
    #    shutil.copy(os.path.join(kernel_dir, ko), modules_dir)

def make_boot_img():
    oldpwd = os.getcwd()
    os.chdir(output_dir)

    imagefile = 'images/boot.img'
    imagefile_tmp = imagefile + '.tmp'
    boot_dir = 'boot'
    #overlays_dir = 'boot/overlays'
    remove_directory(boot_dir)
    remove_file(imagefile)
    shutil.copytree(os.path.join(script_dir, 'boot'), boot_dir)
    #os.mkdir(overlays_dir)
    shutil.copy(
        os.path.join(kernel_dir, 'arch/arm/boot/zImage'),
        boot_dir
    )
    shutil.copy(
        os.path.join(kernel_dir, 'arch/arm/boot/dts/sun8i-h3-nanopi-m1-plus.dtb'),
        boot_dir
    )
    #shutil.copy(
    #    os.path.join(kernel_dir, 'arch/arm/boot/dts/overlays/vc4-kms-v3d.dtbo'),
    #    overlays_dir
    #)
    with open(imagefile_tmp, 'wb') as writer:
        writer.truncate(bootimgsize)
    subprocess.run(F'mkfs.vfat {imagefile_tmp} -n BOOT', shell=True, check=True)
    subprocess.run(F'mcopy -i {imagefile_tmp} {boot_dir}/* ::/', shell=True, check=True)
    os.rename(imagefile_tmp, imagefile)

    os.chdir(oldpwd)

command_table = {
    'kernel': make_kernel_all,
    'modules': install_kernel_modules,
    'bootimg': make_boot_img,
}

command_table[command]()
